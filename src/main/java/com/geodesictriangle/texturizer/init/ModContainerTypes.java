package com.geodesictriangle.texturizer.init;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.objects.items.ContainerWithPallet;
import net.minecraft.inventory.container.ContainerType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;


public class ModContainerTypes {

    public static final DeferredRegister<ContainerType<?>> CONTAINER_TYPES = new DeferredRegister<>(
            ForgeRegistries.CONTAINERS, Texturizer.MODID);

    public static final RegistryObject<ContainerType<ContainerWithPallet>> TEXTURE_WAND = CONTAINER_TYPES
            .register("texturewand", () -> IForgeContainerType.create(ContainerWithPallet::fromNetwork));


}
