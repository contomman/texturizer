package com.geodesictriangle.texturizer.init;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModTileEntityTypes {
    public static final DeferredRegister<TileEntityType<?>> TILE_ENTITY_TYPES = new DeferredRegister<>(
            ForgeRegistries.TILE_ENTITIES, Texturizer.MODID);

    public static final RegistryObject<TileEntityType<GuideTile>> GUIDE_TILE = TILE_ENTITY_TYPES
            .register("guideblock", () -> TileEntityType.Builder.create(GuideTile::new,BlockInit.guideblock.get()).build(null));

}
