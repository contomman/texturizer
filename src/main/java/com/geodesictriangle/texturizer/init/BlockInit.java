package com.geodesictriangle.texturizer.init;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.objects.blocks.GuideBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {

    public static final DeferredRegister<Block> BLOCKS = new DeferredRegister<>(ForgeRegistries.BLOCKS, Texturizer.MODID);

    public static final RegistryObject<Block> guideblock = BLOCKS.register("guideblock", () -> new GuideBlock(Block.Properties.create(Material.GLASS)));

}