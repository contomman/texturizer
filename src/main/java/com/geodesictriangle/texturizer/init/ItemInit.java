package com.geodesictriangle.texturizer.init;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.objects.items.PathWand;
import com.geodesictriangle.texturizer.objects.items.TextureWand;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {

    public static final DeferredRegister<Item> ITEMS = new DeferredRegister<>(ForgeRegistries.ITEMS, Texturizer.MODID);

    public static final RegistryObject<Item> texturewand = ITEMS.register("texturewand", () -> new TextureWand(new Item.Properties().maxStackSize(1).group(Texturizer.texturizerItemGroup.instance)));
    public static final RegistryObject<Item> pathwand = ITEMS.register("pathwand", () -> new PathWand(new Item.Properties().maxStackSize(1).group(Texturizer.texturizerItemGroup.instance)));


}
