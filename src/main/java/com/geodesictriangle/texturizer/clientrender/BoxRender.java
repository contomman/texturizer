/*
Modified From McJty's 1.15 Modding tutorial
 */
package com.geodesictriangle.texturizer.clientrender;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.objects.items.TextureWand;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Texturizer.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class BoxRender {

    @SubscribeEvent
    public static void render(RenderWorldLastEvent event) {
        ClientPlayerEntity player = Minecraft.getInstance().player;

        assert player != null;
        if (player.getHeldItemMainhand().getItem() instanceof TextureWand) {
            drawBox(player, event.getMatrixStack());
        }
    }

    private static void blueLine(IVertexBuilder builder, Matrix4f positionMatrix, BlockPos pos, float dx1, float dy1, float dz1, float dx2, float dy2, float dz2) {
        builder.pos(positionMatrix, pos.getX()+dx1, pos.getY()+dy1, pos.getZ()+dz1)
                .color(0.0f, 0.0f, 1.0f, 1.0f)
                .endVertex();
        builder.pos(positionMatrix, pos.getX()+dx2, pos.getY()+dy2, pos.getZ()+dz2)
                .color(0.0f, 0.0f, 1.0f, 1.0f)
                .endVertex();
    }

    private static void drawBox(ClientPlayerEntity player, MatrixStack matrixStack) {
        IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
        IVertexBuilder builder = buffer.getBuffer(MyRenderType.OVERLAY_LINES);

        ItemStack wand = player.getHeldItemMainhand();

            matrixStack.push();

        Vec3d projectedView = Minecraft.getInstance().gameRenderer.getActiveRenderInfo().getProjectedView();
        matrixStack.translate(-projectedView.x, -projectedView.y, -projectedView.z);

        Matrix4f positionMatrix = matrixStack.getLast().getMatrix();//.getPositionMatrix();

        int x1 = wand.getOrCreateTag().getInt("corner1x");
        int y1 = wand.getOrCreateTag().getInt("corner1y");
        int z1 = wand.getOrCreateTag().getInt("corner1z");
        int x2 = wand.getOrCreateTag().getInt("corner2x");
        int y2 = wand.getOrCreateTag().getInt("corner2y");
        int z2 = wand.getOrCreateTag().getInt("corner2z");
        boolean ValidArea = wand.getOrCreateTag().getBoolean("corner2Valid");
        AxisAlignedBB area = new AxisAlignedBB(x1, y1, z1, x2, y2, z2);

        //BlockPos.Mutable pos = new BlockPos.Mutable();

                    BlockPos bottomCorner = new BlockPos(area.minX,area.minY,area.minZ);
                    int xlen = (int) area.maxX - (int) area.minX + 1;
                    int ylen = (int) area.maxY - (int) area.minY + 1;
                    int zlen = (int) area.maxZ - (int) area.minZ + 1;

        //pos.setPos(px + dx, py + dy, pz + dz);
                    //if (world.getTileEntity(pos) != null) {
                    if (ValidArea) {

                        blueLine(builder, positionMatrix, bottomCorner, 0, 0, 0, xlen, 0, 0);
                        blueLine(builder, positionMatrix, bottomCorner, 0, ylen, 0, xlen, ylen, 0);
                        blueLine(builder, positionMatrix, bottomCorner, 0, 0, zlen, xlen, 0, zlen);
                        blueLine(builder, positionMatrix, bottomCorner, 0, ylen, zlen, xlen, ylen, zlen);

                        blueLine(builder, positionMatrix, bottomCorner, 0, 0, 0, 0, 0, zlen);
                        blueLine(builder, positionMatrix, bottomCorner, xlen, 0, 0, xlen, 0, zlen);
                        blueLine(builder, positionMatrix, bottomCorner, 0, ylen, 0, 0, ylen, zlen);
                        blueLine(builder, positionMatrix, bottomCorner, xlen, ylen, 0, xlen, ylen, zlen);

                        blueLine(builder, positionMatrix, bottomCorner, 0, 0, 0, 0, ylen, 0);
                        blueLine(builder, positionMatrix, bottomCorner, xlen, 0, 0, xlen, ylen, 0);
                        blueLine(builder, positionMatrix, bottomCorner, 0, 0, zlen, 0, ylen, zlen);
                        blueLine(builder, positionMatrix, bottomCorner, xlen, 0, zlen, xlen, ylen, zlen);



        }

        matrixStack.pop();

        RenderSystem.disableDepthTest();
        buffer.finish(MyRenderType.OVERLAY_LINES);
    }
}