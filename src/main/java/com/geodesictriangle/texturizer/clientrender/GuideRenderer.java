package com.geodesictriangle.texturizer.clientrender;

import com.geodesictriangle.texturizer.init.ModTileEntityTypes;
import com.geodesictriangle.texturizer.objects.blocks.GuideBlock;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.*;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Shapes.AbstractShape;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.fml.client.registry.ClientRegistry;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Objects;


public class GuideRenderer extends TileEntityRenderer<GuideTile> {
    public GuideRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    static float px = 0.0625f;
    boolean MINI = false;


    private void add(IVertexBuilder renderer, MatrixStack stack, float x, float y, float z, float u, float v) {
        renderer.pos(stack.getLast().getMatrix(), x, y, z)
                .color(1.0f, 1.0f, 1.0f, 1.0f)
                .tex(u, v)
                .lightmap(0, 240)
                .normal(1, 0, 0)
                .endVertex();
    }

    // this should be true for tileentities which render globally (no render bounding box), such as beacons.
    @Override
    public boolean isGlobalRenderer(GuideTile tile)
    {
        return true;
    }

    @Override
    @ParametersAreNonnullByDefault
    public void render(GuideTile tileEntity, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay) {



        int screenID = tileEntity.ACTIVE_SCREEN;


        List<AbstractModule> modules = tileEntity.modules;


        if(modules!=null) {
            for (AbstractModule module : modules) {
                if (module.activeScreenID == screenID || module.activeScreenID == -1) {

                    renderModule(tileEntity, module, matrixStack, buffer, combinedLight);
                }
            }
        }

        AbstractShape activeshape = tileEntity.shapes.get(tileEntity.ACTIVE_SCREEN);
        List<BlockPos> blocksToRender = activeshape.getBlocks();


        for(BlockPos pos : blocksToRender){
            if(Objects.requireNonNull(tileEntity.getWorld()).getBlockState(tileEntity.getPos().add(pos.getX(),pos.getY(),pos.getZ())).getBlock() == Blocks.AIR) {
                if (MINI) {
                    renderMiniBlock(pos, matrixStack, buffer, combinedLight, combinedOverlay);
                } else {
                    renderBlock(pos, matrixStack, buffer, combinedLight, combinedOverlay);
                }
            }
        }


    }

    void renderBlock(BlockPos pos, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay){

        matrixStack.push();

        matrixStack.translate(pos.getX(), pos.getY(), pos.getZ());
        BlockRendererDispatcher blockRenderer = Minecraft.getInstance().getBlockRendererDispatcher();
        BlockState state = Blocks.COBBLESTONE.getDefaultState();

       // matrixStack.



/*
         matrixStack.scale(0.999f,0.999f,0.999f); //shrink block a tiny bit to stop texture fighting
        matrixStack.translate(0.0005,0.0005,0.0005);
        */



        IBakedModel model = blockRenderer.getModelForState(state);
        int color = Minecraft.getInstance().getBlockColors().getColor(state, null, null, 0);
        float r = (float) (color >> 16 & 255) / 255.0F;
        float g = (float) (color >> 8 & 255) / 255.0F;
        float b = (float) (color & 255) / 255.0F;
        RenderType renderType = MyRenderType.TRANS_BLOCK;
        IVertexBuilder builder = buffer.getBuffer(renderType);

        blockRenderer.getBlockModelRenderer().renderModel(
                matrixStack.getLast(),
                builder,
                state,
                model,
                r,
                g,
                b,
                combinedLight,
                combinedOverlay,
                EmptyModelData.INSTANCE);




       // blockRenderer.renderBlock(state, matrixStack, buffer, combinedLight, combinedOverlay, EmptyModelData.INSTANCE);



        matrixStack.pop();

    }

    void renderMiniBlock(BlockPos pos, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay){
        float scaleFactor = (float)1/16;

        matrixStack.push();

        matrixStack.translate(0.5+scaleFactor*pos.getX(), scaleFactor*pos.getY(), 0.5+scaleFactor*pos.getZ());
        BlockRendererDispatcher blockRenderer = Minecraft.getInstance().getBlockRendererDispatcher();
        BlockState state = Blocks.COBBLESTONE.getDefaultState();

        matrixStack.scale(scaleFactor,scaleFactor,scaleFactor);


        blockRenderer.renderBlock(state, matrixStack, buffer, combinedLight, combinedOverlay, EmptyModelData.INSTANCE);





        matrixStack.pop();

    }




    void renderModule(GuideTile tile,AbstractModule module,MatrixStack matrixStack, IRenderTypeBuffer buffer,int combinedLight){
        if(module instanceof IModuleTexture){
            renderTexture(tile,module,matrixStack,buffer);
        }

        if(module instanceof AbstractTextModule){
            renderText(tile,module,matrixStack,buffer,combinedLight);

        }

    }

    void renderTexture(GuideTile tileEntity,AbstractModule module,MatrixStack matrixStack, IRenderTypeBuffer buffer){
        IVertexBuilder builder = buffer.getBuffer(RenderType.getTranslucent());

        matrixStack.push();

        Direction direction = tileEntity.getBlockState().get(GuideBlock.FACING);
        rotateFacing( matrixStack,direction);

        float z = 0.06f; //Specific to screen model
        //module variables
        float startx = module.x*px;
        float starty = module.y*px;
        float sizex = module.sizex*px;
        float sizey = module.sizey*px;

        FACING facing = ((IModuleTexture)module).getTextureFacing();
        TextureAtlasSprite sprite = ((IModuleTexture) module).getTexture();
        //TEXTURE DIRECTION KEY
        switch(facing) {
            case UP:
            default:

                add(builder, matrixStack, startx + sizex, starty + sizey, z, sprite.getMinU(), sprite.getMinV());
                add(builder, matrixStack, startx, starty + sizey, z, sprite.getMaxU(), sprite.getMinV());
                add(builder, matrixStack, startx, starty, z, sprite.getMaxU(), sprite.getMaxV());
                add(builder, matrixStack, startx + sizex, starty, z, sprite.getMinU(), sprite.getMaxV());


            break;


            case DOWN:
                add(builder, matrixStack, startx, starty, z, sprite.getMinU(), sprite.getMinV());
                add(builder, matrixStack, startx + sizex, starty, z, sprite.getMaxU(), sprite.getMinV());
                add(builder, matrixStack, startx + sizex, starty + sizey, z, sprite.getMaxU(), sprite.getMaxV());
                add(builder, matrixStack, startx, starty + sizey, z, sprite.getMinU(), sprite.getMaxV());
            break;

            case RIGHT:
                add(builder, matrixStack, startx + sizex, starty, z, sprite.getMinU(), sprite.getMinV());
                add(builder, matrixStack, startx + sizex, starty + sizey, z, sprite.getMaxU(), sprite.getMinV());
                add(builder, matrixStack, startx, starty + sizey, z, sprite.getMaxU(), sprite.getMaxV());
                add(builder, matrixStack, startx, starty, z, sprite.getMinU(), sprite.getMaxV());
            break;

            case LEFT:
                add(builder, matrixStack, startx, starty + sizey, z, sprite.getMinU(), sprite.getMinV());
                add(builder, matrixStack, startx, starty, z, sprite.getMaxU(), sprite.getMinV());
                add(builder, matrixStack, startx + sizex, starty, z, sprite.getMaxU(), sprite.getMaxV());
                add(builder, matrixStack, startx + sizex, starty + sizey, z, sprite.getMinU(), sprite.getMaxV());
            break;
        }


        matrixStack.pop();

    }

    void renderText(GuideTile tileEntity,AbstractModule module,MatrixStack matrixStack, IRenderTypeBuffer buffer,int combinedLight) {
        //((TextModule)module).setText();
        matrixStack.push();

        Direction direction = tileEntity.getBlockState().get(GuideBlock.FACING);
        rotateFacing( matrixStack,direction);

        int j = 0;//Integer.MIN_VALUE; //background????
        int color = 123456;//4210752;
        //int k = 553648127;
        float xstretch=1;
        float ystretch=1;
        int unknownscalefactor=1; //dont know what this does

        ((AbstractTextModule) module).updateText();
        String text = ((AbstractTextModule)module).getText();

        int pxLen = (int) (text.length()*1.25);

        if(pxLen > module.sizex) {
            float scale = module.sizex / pxLen;
            xstretch = scale;
            ystretch = scale;
        }




        matrixStack.translate(module.x*px,module.y*px,px*1);
        matrixStack.scale(xstretch*0.010416667F, -ystretch*0.010416667F, unknownscalefactor*0.010416667F);

        FontRenderer fontRenderer = Minecraft.getInstance().fontRenderer;



        fontRenderer.renderString(text, 0, 0, color, false, matrixStack.getLast().getMatrix(), buffer, false, j, combinedLight);
        matrixStack.pop();
    }
/*
Takes our rendering stack and rotates it to whatever direction the block is facing
 */
    private void rotateFacing(MatrixStack matrixStack, Direction direction){
            switch (direction) {
                case WEST:
                    matrixStack.translate(0.5D, 0.5D, 0.5D);
                    matrixStack.rotate(Vector3f.YP.rotationDegrees(270));
                    matrixStack.translate(-0.5D, -0.5D, -0.5D);
                    break;
                case NORTH:
                    matrixStack.translate(0.5D, 0.5D, 0.5D);
                    matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
                    matrixStack.translate(-0.5D, -0.5D, -0.5D);
                    break;
                case EAST:
                    matrixStack.translate(0.5D, 0.5D, 0.5D);
                    matrixStack.rotate(Vector3f.YP.rotationDegrees(90));
                    matrixStack.translate(-0.5D, -0.5D, -0.5D);
                    break;
                case SOUTH:
                default:
                    break;
            }
        }





    public static void register(){
        ClientRegistry.bindTileEntityRenderer(ModTileEntityTypes.GUIDE_TILE.get(), GuideRenderer::new);
    }
}
