package com.geodesictriangle.texturizer.clientrender;


import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

//Not used right now
//Renders a block of Obsidian at [0,70,0]

/*
@Mod.EventBusSubscriber(Dist.CLIENT)
public class BlockRender{
    @SuppressWarnings("deprecation")
    @SubscribeEvent
    public static void renderWorldLastEvent(RenderWorldLastEvent event)
    {
        if(event.getPhase() != EventPriority.NORMAL)
            return;

        Entity entity = Minecraft.getInstance().getRenderViewEntity();

        if(entity == null)
            return;


        Vec3d projectedView = Minecraft.getInstance().gameRenderer.getActiveRenderInfo().getProjectedView();

        MinecraftServer server = Minecraft.getInstance().getIntegratedServer();
        World world = DimensionManager.getWorld(server, DimensionType.OVERWORLD, false, true);
        MatrixStack matrixStack = event.getMatrixStack();
        BlockRendererDispatcher blockRendererDispatcher = Minecraft.getInstance().getBlockRendererDispatcher();

        BlockState blockState = Blocks.OBSIDIAN.getDefaultState();

        IRenderTypeBuffer.Impl renderTypeBuffer = IRenderTypeBuffer.getImpl(Tessellator.getInstance().getBuffer());

        matrixStack.push();
        matrixStack.translate(-projectedView.x, -projectedView.y+70, -projectedView.z);

        RenderType renderType = RenderTypeLookup.getRenderType(blockState);
        blockRendererDispatcher.renderModel(blockState, new BlockPos(0, 70, 0), world, matrixStack, renderTypeBuffer.getBuffer(renderType), false, world.getRandom());

        matrixStack.pop();

        renderTypeBuffer.finish();
    }
}

 */