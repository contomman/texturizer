package com.geodesictriangle.texturizer.client;

import com.geodesictriangle.texturizer.Texturizer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_M;

@Mod.EventBusSubscriber(modid = Texturizer.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class ClientProxy {

    public static final KeyBinding toggleWandMode = new KeyBinding(Texturizer.MODID + ".key.toggleWandMode", GLFW_KEY_M, "key.categories." + Texturizer.MODID);


    static {
        ClientRegistry.registerKeyBinding(toggleWandMode);

    }
}