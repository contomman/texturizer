package com.geodesictriangle.texturizer.client;


import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.network.message.MessageModeToggle;
import com.geodesictriangle.texturizer.objects.items.AbstractSwapWand;
import com.geodesictriangle.texturizer.objects.items.TextureWand;
import com.geodesictriangle.texturizer.network.PacketHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Texturizer.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)//, value = Dist.CLIENT)
public class KeyInputHandler {
    @SubscribeEvent
    public static void onKeyInput(InputEvent.KeyInputEvent event) {
//Below code works to detect keys and do actions client side, but needs to be modified to send the packets to the server side


        if (event.getAction() == 1) //on press
        {
            if (event.getKey() == ClientProxy.toggleWandMode.getKey().getKeyCode()) {


                Minecraft mc = Minecraft.getInstance();


                assert mc.player != null;
                if (mc.player.getHeldItemMainhand().getItem() instanceof AbstractSwapWand) {

                    //((TextureWand) mc.player.getHeldItemMainhand().getItem()).toggleMode(mc.player, mc.player.getHeldItemMainhand());
                    PacketHandler.INSTANCE.sendToServer(new MessageModeToggle());
                }
            }



        }


    }
}