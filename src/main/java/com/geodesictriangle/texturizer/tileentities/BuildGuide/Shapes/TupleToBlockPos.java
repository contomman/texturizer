package com.geodesictriangle.texturizer.tileentities.BuildGuide.Shapes;

import net.minecraft.util.Direction;
import net.minecraft.util.Tuple;
import net.minecraft.util.math.BlockPos;

public class TupleToBlockPos {
    public static BlockPos tupleToBlockPos(Tuple<Integer,Integer> tuple, Direction FACING){
        switch(FACING){
            case EAST:
            case WEST:
                return new BlockPos(0,tuple.getB(), tuple.getA());
            case NORTH:
            case SOUTH:
            default:
                return new BlockPos(tuple.getA(),tuple.getB(),0);
        }
    }
}
