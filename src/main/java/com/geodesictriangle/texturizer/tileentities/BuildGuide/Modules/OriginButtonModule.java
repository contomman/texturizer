package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;



public class OriginButtonModule extends AbstractButtonModule implements IModuleTexture {
    //TEXTURE DIRECTION KEY

    int direction;
    FACING facing;
    int mode;
    int linkedIntID;
    GuideTile tile;
    final static int originUP = 0;
    final int originDOWN = 1;
    final int originLEFT = 2;
    final int originRIGHT = 3;
    final int originFORWARD = 4;
    final int originBACK = 5;

    public static final ResourceLocation ARROW_TEXTURE_UP = new ResourceLocation(Texturizer.MODID, "buildingguide/arrow");
    public static final ResourceLocation ARROW_TEXTURE_DOWN = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowdown");
    public static final ResourceLocation ARROW_TEXTURE_LEFT = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowleft");
    public static final ResourceLocation ARROW_TEXTURE_RIGHT = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowright");
    TextureAtlasSprite spriteup = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_UP);
    TextureAtlasSprite spritedown = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_DOWN);
    TextureAtlasSprite spriteleft = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_LEFT);
    TextureAtlasSprite spriteright = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_RIGHT);


    public OriginButtonModule(GuideTile parentTile, float x, float y, float sizex, float sizey, int activeScreenID,int facing) {
        super(parentTile,x, y, sizex, sizey, activeScreenID);
        this.direction = facing;
        this.mode = mode;
        this.linkedIntID = linkedIntID;
        this.tile=parentTile;
        switch (direction){
            case originUP:
            default:
                this.facing = FACING.UP;
                break;
            case originDOWN:
                this.facing = FACING.DOWN;
                break;
            case originLEFT:
                this.facing = FACING.LEFT;
                break;
            case originRIGHT:
                this.facing = FACING.RIGHT;
                break;
            case originFORWARD:
                this.facing = FACING.UP;
                break;
            case originBACK:
                this.facing = FACING.DOWN;
                break;
        }
    }



    @Override
    public TextureAtlasSprite getTexture(){
        switch (direction){
            case originUP:
            case originFORWARD:
            default:
            return spriteup;
            case originDOWN:
            case originBACK:
                return spritedown;
            case originLEFT:
                return spriteleft;
            case originRIGHT:
                return spriteright;

        }
    }


    @Override
    public FACING getTextureFacing() {
        return FACING.UP;
    }

    @Override
    public boolean isClicked(float clickX, float clickY) {
        return (x < clickX*16 && clickX*16 < x+sizex && y<clickY*16 && clickY*16 < y+sizey);
    }

    @Override
    public void onClick() {
        int amount = 1;
        switch (direction){
            case originUP:
            default:
                tile.originOffset.moveRelY(amount);
                break;
            case originDOWN:
                tile.originOffset.moveRelY(-amount);
                break;
            case originLEFT:
                tile.originOffset.moveRelX(-amount);
                break;
            case originRIGHT:
                tile.originOffset.moveRelX(amount);
                break;
            case originFORWARD:
                tile.originOffset.moveRelZ(amount);
                break;
            case originBACK:
                tile.originOffset.moveRelZ(-amount);
                break;
        }
        super.onClick();

    }

}
