package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;

import static com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.FACING.*;


public class ArrowButtonModule extends AbstractButtonModule implements IModuleTexture {


    FACING facing;
    int mode;
    int linkedIntID;
    GuideTile tile;

    public static final ResourceLocation ARROW_TEXTURE_UP = new ResourceLocation(Texturizer.MODID, "buildingguide/arrow");
    public static final ResourceLocation ARROW_TEXTURE_DOWN = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowdown");
    public static final ResourceLocation ARROW_TEXTURE_LEFT = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowleft");
    public static final ResourceLocation ARROW_TEXTURE_RIGHT = new ResourceLocation(Texturizer.MODID, "buildingguide/arrowright");
    TextureAtlasSprite spriteup = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_UP);
    TextureAtlasSprite spritedown = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_DOWN);
    TextureAtlasSprite spriteleft = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_LEFT);
    TextureAtlasSprite spriteright = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(ARROW_TEXTURE_RIGHT);



    public ArrowButtonModule(GuideTile parentTile, float x, float y, float sizex, float sizey, int activeScreenID,int mode, FACING facing,int linkedIntID) {
        super(parentTile,x, y, sizex, sizey, activeScreenID);
        this.facing = facing;
        this.mode = mode;
        this.linkedIntID = linkedIntID;
        this.tile=parentTile;
    }



    @Override
    public TextureAtlasSprite getTexture(){
        switch (facing) {
            case UP:
            default:
                return spriteup;
            case DOWN:
                return spritedown;
            case LEFT:
                return spriteleft;
            case RIGHT:
                return spriteright;

        }
    }


    @Override
    public FACING getTextureFacing() {
        return UP;
    }

    @Override
    public boolean isClicked(float clickX, float clickY) {
        return (x < clickX*16 && clickX*16 < x+sizex && y<clickY*16 && clickY*16 < y+sizey);
    }

    @Override
    public void onClick() {
        tile.changeVar(linkedIntID,mode);
        super.onClick();

    }
}
