package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.ResourceLocation;

import static com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.FACING.UP;

public class BuildButtonModule extends AbstractButtonModule implements IModuleTexture {


    FACING facing;
    int mode;
    int linkedIntID;
    GuideTile tile;

    public static final ResourceLocation BUILD_TEXTURE = new ResourceLocation(Texturizer.MODID, "buildingguide/build");
    TextureAtlasSprite sprite = Minecraft.getInstance().getAtlasSpriteGetter(AtlasTexture.LOCATION_BLOCKS_TEXTURE).apply(BUILD_TEXTURE);


    public BuildButtonModule(GuideTile parentTile, float x, float y, float sizex, float sizey, int activeScreenID) {
        super(parentTile,x, y, sizex, sizey, activeScreenID);
        this.tile=parentTile;
    }



    @Override
    public TextureAtlasSprite getTexture(){
        return sprite;
    }


    @Override
    public FACING getTextureFacing() {
        return UP;
    }

    @Override
    public boolean isClicked(float clickX, float clickY) {
        return (x < clickX*16 && clickX*16 < x+sizex && y<clickY*16 && clickY*16 < y+sizey);
    }

    @Override
    public void onClick() {
        Texturizer.LOGGER.info("Building");
        tile.build();
        super.onClick();


    }
}