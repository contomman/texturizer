package com.geodesictriangle.texturizer.tileentities.BuildGuide;

import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.init.ModTileEntityTypes;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.AbstractModule;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.ModuleList;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Shapes.*;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.geodesictriangle.texturizer.objects.blocks.GuideBlock.FACING;
import static com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.ModuleList.*;


public class GuideTile extends TileEntity implements ITickableTileEntity {


    //Flag for the builder to place blocks
    boolean BUILDING = false;



    ModuleList moduleList = new ModuleList(this);
    public List<AbstractModule> modules;

    //What screen to draw TODO save as nbt so it is persistent
    public Integer ACTIVE_SCREEN = 0;
    private int ACTIVE_SCREEN_MAX;
    private int ACTIVE_SCREEN_MIN = 0;

    //All the different Shapes that can be drawn
    public List<AbstractShape> shapes = new ArrayList<>();

    public int ALWAYS_ACTIVE = -1;

    public Square square = new Square(this);
    public Circle circle = new Circle(this);
    public SemiCircleArch semiarch = new SemiCircleArch(this);
    public GothicArch gothicArch = new GothicArch(this);
    public DepressedArch depressedArch = new DepressedArch(this);
    public InflectedArch inflectedArch = new InflectedArch(this);
    public ThreefoilArch threefoilArch = new ThreefoilArch(this);
    public PointedTrefoilArch pointedTrefoilArch = new PointedTrefoilArch(this);
    public OriginOffset originOffset = new OriginOffset(this);







    public GuideTile(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);

    }


    public GuideTile(){
        this(ModTileEntityTypes.GUIDE_TILE.get());
        modules = moduleList.getAllModules();

        //ACTIVE_SCREEN = 0;

        //populate shapes list
        shapes.add(square);
        shapes.add(circle);
        shapes.add(semiarch);
        shapes.add(gothicArch);
        shapes.add(depressedArch);
        shapes.add(inflectedArch);
        shapes.add(threefoilArch);
        shapes.add(pointedTrefoilArch);




        ACTIVE_SCREEN_MAX = shapes.size()-1;
    }



    public void changeVar(int varID, int action){
        AbstractShape activeshape = this.shapes.get(ACTIVE_SCREEN);

            if (varID == -1) {
                if (action == INC_MODE) {
                    ACTIVE_SCREEN++;
                    if(ACTIVE_SCREEN > ACTIVE_SCREEN_MAX){ACTIVE_SCREEN = ACTIVE_SCREEN_MIN;}
                }
                if (action == DEC_MODE) {
                    ACTIVE_SCREEN--;
                    if(ACTIVE_SCREEN < ACTIVE_SCREEN_MIN){ACTIVE_SCREEN = ACTIVE_SCREEN_MAX;}

                }
            } else {

            if (action == INC_MODE) {
                activeshape.setValue(varID, activeshape.getValue(varID) + 1);
            }

            if (action == DEC_MODE) {
                activeshape.setValue(varID, activeshape.getValue(varID) - 1);
            }
            if (action == INC2_MODE) {
                    activeshape.setValue(varID, activeshape.getValue(varID) + 2);
                }
            if (action == DEC2_MODE) {
                    activeshape.setValue(varID, activeshape.getValue(varID) - 2);
                }
        }

    }





    /**
     * Don't render the object if the player is too far away
     * @return the maximum distance squared at which the TER should render
     */
    @Override
    public double getMaxRenderDistanceSquared()
    {
        final int MAXIMUM_DISTANCE_IN_BLOCKS = 1024;
        return MAXIMUM_DISTANCE_IN_BLOCKS * MAXIMUM_DISTANCE_IN_BLOCKS;
    }

    /** Return an appropriate bounding box enclosing the TER
     * This method is used to control whether the TER should be rendered or not, depending on where the player is looking.
     * The default is the AABB for the parent block, which might be too small if the TER renders outside the borders of the
     *   parent block.
     * If you get the boundary too small, the TER may disappear when you aren't looking directly at it.
     * @return an appropriately size AABB for the TileEntity
     */
    @Override
    public AxisAlignedBB getRenderBoundingBox()
    {
        // if your render should always be performed regardless of where the player is looking, use infinite
        //   Your should also change TileEntitySpecialRenderer.isGlobalRenderer().
        AxisAlignedBB infiniteExample = INFINITE_EXTENT_AABB;

        // our gem will stay above the block, up to 1 block higher, so our bounding box is from [x,y,z] to  [x+1, y+2, z+1]
       // AxisAlignedBB aabb = new AxisAlignedBB(getPos(), getPos().add(1, 2, 1));
        return INFINITE_EXTENT_AABB;
    }

    public void build() {
        BUILDING = true;

    }

    public Block getAndRemoveInvBlock(){
        BlockPos tilePos = this.getPos();
        Direction facing =  Objects.requireNonNull(this.getWorld()).getBlockState(tilePos).get(FACING);
        BlockPos invPos;

        switch (facing){

            case NORTH:
            default:
                 invPos = tilePos.add(0,0,1);
                break;
            case EAST:
                 invPos = tilePos.add(-1,0,0);
                break;
            case SOUTH:
                 invPos = tilePos.add(0,0,-1);
                break;
            case WEST:
                 invPos = tilePos.add(1,0,0);
                break;

        }
        if(this.getWorld().getBlockState(invPos).hasTileEntity()){
            TileEntity potentialContainer = this.getWorld().getTileEntity(invPos);
            IItemHandler inv = potentialContainer.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).orElse(null);
            if(inv != null){
                for(int slot  = 0; slot<inv.getSlots();slot++){
                    if(inv.getStackInSlot(slot).getItem() instanceof BlockItem){
                        Texturizer.LOGGER.info("block {} found in slot {}",inv.getStackInSlot(slot),slot);
                        Block block = ((BlockItem) inv.getStackInSlot(slot).getItem()).getBlock();
                        inv.getStackInSlot(slot).shrink(1);
                        return block;

                    }

                }
            }

        }
        return Blocks.AIR;
    }

    @Override
    public void tick() {

        if(BUILDING){

            if (!world.isRemote) {
                AbstractShape activeshape = this.shapes.get(ACTIVE_SCREEN);
                List<BlockPos> blockList = activeshape.getBlocks();
                for (BlockPos pos : blockList) {
                    BlockPos tilePos = this.getPos();
                    BlockPos abspos = tilePos.add(pos.getX(),pos.getY(),pos.getZ());
                    if (world.getBlockState(abspos).getBlock() == Blocks.AIR) {
                        Block block = getAndRemoveInvBlock();
                        if (block != Blocks.AIR) {
                            world.setBlockState(abspos, block.getDefaultState());
                        } else {
                            BUILDING = false;
                        }
                    }

                }
                BUILDING = false;

            }

        }
    }
//todo Serialize data and store in nbt


    }



