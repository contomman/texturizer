package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

public interface IModuleClickable {
    boolean isClicked(float x,float y);
    void onClick();
}
