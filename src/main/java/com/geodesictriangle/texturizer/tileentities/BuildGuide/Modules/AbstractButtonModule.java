package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;

public abstract class AbstractButtonModule extends AbstractModule implements IModuleClickable{
    public AbstractButtonModule(GuideTile parent,float x, float y, float sizex, float sizey, int activeScreenID) {
        super(parent,x, y, sizex, sizey, activeScreenID);

    }


    @Override
    public void onClick() {
       // tile.resetText();
    }
}
