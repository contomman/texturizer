package com.geodesictriangle.texturizer.tileentities.BuildGuide.Shapes;

import com.geodesictriangle.texturizer.tileentities.BuildGuide.GuideTile;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;

import java.util.Objects;

import static com.geodesictriangle.texturizer.objects.blocks.GuideBlock.FACING;

public class OriginOffset {
    public BlockPos originOffset;
    public Direction facing;
    public GuideTile guideTile;
    public OriginOffset(GuideTile tile){
        originOffset = new BlockPos(0,0,0);
        guideTile = tile;

    }

    public void moveRelX(int amount){
        facing = Objects.requireNonNull(guideTile.getWorld()).getBlockState(guideTile.getPos()).get(FACING);

        switch (facing){
            case NORTH:
            default:
                moveAbsX(-amount);
                break;
            case EAST:
                moveAbsZ(-amount);
                break;
            case SOUTH:
                moveAbsX(amount);
                break;
            case WEST:
                moveAbsZ(amount);
                break;
        }

    }
    public void moveRelY(int amount){
        moveAbsY(amount);
    }
    public void moveRelZ(int amount){
        facing = Objects.requireNonNull(guideTile.getWorld()).getBlockState(guideTile.getPos()).get(FACING);
        switch (facing){
            case NORTH:
            default:
                moveAbsZ(amount);
                break;
            case EAST:
                moveAbsX(-amount);
                break;
            case SOUTH:
                moveAbsZ(-amount);
                break;
            case WEST:
                moveAbsX(amount);
                break;
        }
    }

    private void moveAbsX(int amount){
        originOffset = originOffset.add(amount,0,0);
    }
    private void moveAbsY(int amount){
        originOffset = originOffset.add(0,amount,0);
    }
    private void moveAbsZ(int amount){
        originOffset = originOffset.add(0,0,amount);
    }
}
