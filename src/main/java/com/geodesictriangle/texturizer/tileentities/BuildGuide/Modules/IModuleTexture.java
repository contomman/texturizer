package com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules;

import net.minecraft.client.renderer.texture.TextureAtlasSprite;

public interface IModuleTexture {
    TextureAtlasSprite getTexture();
    FACING getTextureFacing();

}
