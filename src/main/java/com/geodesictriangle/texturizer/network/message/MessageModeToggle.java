package com.geodesictriangle.texturizer.network.message;


import com.geodesictriangle.texturizer.objects.items.AbstractSwapWand;
import com.geodesictriangle.texturizer.objects.items.TextureWand;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class MessageModeToggle {

   // private final String id;
    //private final DimensionType type;
   // private final BlockPos pos;

    public MessageModeToggle(PacketBuffer buf) {
       // id = buf.readString();
        //type = DimensionType.getById(buf.readInt());
        //pos = buf.readBlockPos();
    }

    public MessageModeToggle(){//String id){//, DimensionType type, BlockPos pos) {
        //this.id = id;
        //this.type = type;
        //this.pos = pos;
    }

    public void toBytes(PacketBuffer buf) {
       // buf.writeString(id);
        //buf.writeInt(type.getId());
        //buf.writeBlockPos(pos);
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {

            PlayerEntity player = ctx.get().getSender();
            //if ((player.getHeldItemMainhand().getItem() != null) && (player.getHeldItemMainhand().getItem() instanceof AbstractSwapWand)) {

                ((AbstractSwapWand) player.getHeldItemMainhand().getItem()).toggleMode(player, player.getHeldItemMainhand());
           // }


        });

        ctx.get().setPacketHandled(true);
    }

}