package com.geodesictriangle.texturizer.objects.blocks;

import com.geodesictriangle.texturizer.init.ModTileEntityTypes;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.*;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.AbstractModule;
import com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.IModuleClickable;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;
import java.util.stream.Stream;

@MethodsReturnNonnullByDefault
public class GuideBlock extends Block {

    public static final DirectionProperty FACING = HorizontalBlock.HORIZONTAL_FACING;


    public GuideBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(FACING, Direction.NORTH));

    }



    @Override
    public boolean hasTileEntity(BlockState state){
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world){
        return ModTileEntityTypes.GUIDE_TILE.get().create();
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    /**
     * Returns the blockstate with the given rotation from the passed blockstate. If
     * inapplicable, returns the passed blockstate.
     */
    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    /**
     * Returns the blockstate with the given mirror of the passed blockstate. If
     * inapplicable, returns the passed blockstate.
     */
    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }


    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player,
                                             Hand handIn, BlockRayTraceResult result) {
        // if (!worldIn.isRemote()) {
       // ServerWorld serverWorld = (ServerWorld) world;

       // GuideTile tileEntity = (GuideTile) serverWorld.getTileEntity(pos);

          //  }

         GuideTile tileEntity = (GuideTile) world.getTileEntity(pos);
        assert tileEntity != null;
        List<AbstractModule> modules = tileEntity.modules;//moduleList.getAllModules();


                for (AbstractModule module : modules) {
                    if (module.activeScreenID == tileEntity.ACTIVE_SCREEN || module.activeScreenID == -1) {
                        if (module instanceof IModuleClickable) {
                            ClickModuleFacing(module, result.getHitVec(), pos, state.get(FACING));
                        }
                    }
                }








            //Texturizer.LOGGER.info("Clicked Model {} {} {}",localx,localy,localz);


        return ActionResultType.SUCCESS;
    }
/*
Changes blockhit coords to local screen coords
 */
    private void ClickModuleFacing(AbstractModule moduleClickable,Vec3d hitVec,BlockPos pos, Direction facing){
        IModuleClickable module = (IModuleClickable) moduleClickable;

        float x = (float) (hitVec.x - pos.getX());
        float y = (float) (hitVec.y - pos.getY());
        float z = (float) (hitVec.z - pos.getZ());
        float relX;
        switch (facing) {
            case NORTH:
                relX = 1-x;
                break;
            case EAST:
                relX = 1-z;
                break;
            case WEST:
                relX = z;
                break;
            case SOUTH:
            default:
                relX = x;
                break;
        }
        if(module.isClicked(relX,y)){
            module.onClick();
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(FACING)) {
            case NORTH:
                return SHAPE_N;
            case SOUTH:
                return SHAPE_S;
            case EAST:
                return SHAPE_E;
            case WEST:
                return SHAPE_W;
            default:
                return SHAPE_N;
        }
    }
VoxelShape SHAPE_S = Stream.of(
        Block.makeCuboidShape(0.10000000000000142, 0.10000000000000142, 0.09999999999999998, 15.900000000000002, 15.900000000000002, 0.9),
        Block.makeCuboidShape(0, 0, 0, 16, 1, 1),
        Block.makeCuboidShape(0, 1, 0, 1, 15, 1),
        Block.makeCuboidShape(15, 1, 0, 16, 15, 1),
        Block.makeCuboidShape(0, 15, 0, 16, 16, 1),
        Block.makeCuboidShape(1, 1, 0.8, 15, 15, 1)
).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();

VoxelShape SHAPE_E =Stream.of(
        Block.makeCuboidShape(0.10000000000000142, 0.10000000000000142, 0.10000000000000142, 0.8999999999999986, 15.900000000000002, 15.900000000000002),
        Block.makeCuboidShape(0, 0, 0, 1, 1, 16),
        Block.makeCuboidShape(0, 1, 0, 1, 15, 1),
        Block.makeCuboidShape(0, 1, 15, 1, 15, 16),
        Block.makeCuboidShape(0, 15, 0, 1, 16, 16),
        Block.makeCuboidShape(0, 1, 1, 0.1999999999999993, 15, 15)
).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();


VoxelShape SHAPE_N =Stream.of(
        Block.makeCuboidShape(0.10000000000000142, 0.10000000000000142, 15.100000000000001, 15.900000000000002, 15.900000000000002, 15.899999999999999),
        Block.makeCuboidShape(0, 0, 15, 16, 1, 16),
        Block.makeCuboidShape(0, 1, 15, 1, 15, 16),
        Block.makeCuboidShape(15, 1, 15, 16, 15, 16),
        Block.makeCuboidShape(0, 15, 15, 16, 16, 16),
        Block.makeCuboidShape(1, 1, 15.8, 15, 15, 16)
).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();


VoxelShape SHAPE_W =Stream.of(
        Block.makeCuboidShape(15.100000000000001, 0.10000000000000142, 0.09999999999999787, 15.899999999999999, 15.900000000000002, 15.899999999999999),
        Block.makeCuboidShape(15, 0, 0, 16, 1, 16),
        Block.makeCuboidShape(15, 1, 15, 16, 15, 16),
        Block.makeCuboidShape(15, 1, 0, 16, 15, 1),
        Block.makeCuboidShape(15, 15, 0, 16, 16, 16),
        Block.makeCuboidShape(15.8, 1, 1, 16, 15, 15)
).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();




}
