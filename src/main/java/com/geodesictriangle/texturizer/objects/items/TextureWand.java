/*
WARNING! This code is awful.
I have no idea what I am doing, and was very.... creative.... with the implementations
If you are reading this code and know the correct way to do things, I am open to critique. Just remember I am a Java brainlet so Explain Like Im 5 if possible
The code was much nicer before I had to put all of the data within NBT tags so multiple instances of the item could work in multiplayer

If you are here to look at how I did this, look at Botania's Rod of the Shifting Crust code instead.
I took 'heavy inspiration' from it, and took the concepts of their well done methods, and implemented them poorly.
 */

package com.geodesictriangle.texturizer.objects.items;

import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;


@MethodsReturnNonnullByDefault
public class TextureWand extends AbstractSwapWand {

    public TextureWand(Properties properties) {
        super(properties);
    }



    //Available States
    Integer SELECTING_CORNER_ONE = 0;
    Integer SELECTING_CORNER_TWO = 1;
    Integer SWAPPING = 2;

    //Available Modes
    Integer HOTBAR = 0;
    Integer INTERNAL = 1;

    Integer MAXIMUM_BLOCKS = 1000000;

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        World world = context.getWorld();
        BlockPos pos = context.getPos();
        PlayerEntity player = context.getPlayer();
        ItemStack stack = context.getItem();


        if (!world.isRemote) {

            int state = stack.getOrCreateTag().getInt("state");




            if (player != null && player.isCrouching()) {
                    if (state == SELECTING_CORNER_ONE) {
                        player.sendMessage(new StringTextComponent("Corner 1 set"));
                        setCorner1(stack,pos);
                        stack.getOrCreateTag().putBoolean("corner2Valid",false);
                        //state = SELECTING_CORNER_TWO;
                        stack.getOrCreateTag().putInt("state",SELECTING_CORNER_TWO);
                        return ActionResultType.SUCCESS;


                    } else if (state == SELECTING_CORNER_TWO) {
                        player.sendMessage(new StringTextComponent("Corner 2 set"));
                        setCorner2(stack,pos);

                        stack.getOrCreateTag().putBoolean("corner2Valid",true);
                        //state = SELECTING_CORNER_ONE;
                        stack.getOrCreateTag().putInt("state",SELECTING_CORNER_ONE);
                        return ActionResultType.SUCCESS;
                    }
            }

            // Corners are set
            assert player != null;
            if (stack.getOrCreateTag().getBoolean("corner2Valid") && !player.isCrouching()) {
                if(withinArea(stack,pos)){

                    swapArea(context); //TODO LOTS of side effects and state changes in this function, should be broken up
                }

            }



        }
        return ActionResultType.SUCCESS;
    }









    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean equipped)
    {

        if(!world.isRemote()) {

                if (!(entity instanceof PlayerEntity))
                    return;
                PlayerEntity player = (PlayerEntity) entity;
                Integer state = stack.getOrCreateTag().getInt("state");

            if (state.equals(SWAPPING)) {


                //These always return valid, but will set the state to not swap if there are no valid targets left or if
                //This is not a good way to do this TODO fix this
                BlockPos nextPos = getNextSwapPos(world,stack);
                Block nextBlock = getNextBlock(stack,player);
                setLastVisited(stack,nextPos);


                state = stack.getOrCreateTag().getInt("state");

                if (state.equals(SWAPPING)) {
                    if(nextBlock != Blocks.AIR) { //Air block means skip this spot
                        swapBlock(world, nextPos, !player.abilities.isCreativeMode, nextBlock);
                    }

                }
                }

                }

    }

    public void swapBlock(World world, BlockPos pos, boolean dropBlock, Block swapblock){
        if(!world.isRemote) {
            //destroyBlockNoParticles(destroypos,!player.abilities.isCreativeMode,(Entity) null, world);
            world.destroyBlock(pos, dropBlock);
            world.setBlockState(pos, swapblock.getDefaultState());
        }
    }

    public Block getNextBlock(ItemStack stack, PlayerEntity player){
        Integer mode = stack.getOrCreateTag().getInt("mode");
        List<Block> blocklist = getBlockList(player, stack);

        if(blocklist.size() > 0) {
            int randomblock = (int) (Math.random() * blocklist.size());
            Block block = blocklist.get(randomblock);

            if(block != Blocks.AIR){
            if(player.abilities.isCreativeMode){
                return block;
            }

            boolean HasBlock = false;

            if(mode.equals(INTERNAL)){
                HasBlock = wandInvRemoveItem(stack, block.asItem()); // Also removes item
            }
            if(mode.equals(HOTBAR) || !HasBlock) {
                HasBlock = playerInvRemoveItem(player, block.asItem()); // Also removes item
            }

            if(!HasBlock){
                stack.getOrCreateTag().putInt("state",SELECTING_CORNER_ONE);
                player.sendMessage(new StringTextComponent("Ran Out of "+ block.asItem() +". Pausing"));
            }
                return block;
            }

        }
        return Blocks.AIR;
        //randomblock = 1;
        //return Blocks.STONE;

    }


    public void setCorner1(ItemStack stack, BlockPos pos){
        stack.getOrCreateTag().putInt("corner1x", pos.getX());
        stack.getOrCreateTag().putInt("corner1y", pos.getY());
        stack.getOrCreateTag().putInt("corner1z", pos.getZ());

    }
    public void setCorner2(ItemStack stack, BlockPos pos){
        stack.getOrCreateTag().putInt("corner2x", pos.getX());
        stack.getOrCreateTag().putInt("corner2y", pos.getY());
        stack.getOrCreateTag().putInt("corner2z", pos.getZ());
        setSwapStart(stack);
    }
    public void setLastVisited(ItemStack stack, BlockPos pos){
        stack.getOrCreateTag().putInt("lastX", pos.getX());
        stack.getOrCreateTag().putInt("lastY", pos.getY());
        stack.getOrCreateTag().putInt("lastZ", pos.getZ());
    }

    public void setSwapStart(ItemStack stack){
        int x1 = stack.getOrCreateTag().getInt("corner1x");
        int y1 = stack.getOrCreateTag().getInt("corner1y");
        int z1 = stack.getOrCreateTag().getInt("corner1z");
        int x2 = stack.getOrCreateTag().getInt("corner2x");
        int y2 = stack.getOrCreateTag().getInt("corner2y");
        int z2 = stack.getOrCreateTag().getInt("corner2z");
        AxisAlignedBB area = new AxisAlignedBB(x1, y1, z1, x2, y2, z2);


        //Set position to start checking for swapping
        stack.getOrCreateTag().putInt("lastX", (int) area.minX -1);
        stack.getOrCreateTag().putInt("lastY", (int) area.minY);
        stack.getOrCreateTag().putInt("lastZ", (int) area.minZ);
    }

    public BlockPos getCorner1(ItemStack stack){
        int x = stack.getOrCreateTag().getInt("corner1x");
        int y = stack.getOrCreateTag().getInt("corner1y");
        int z = stack.getOrCreateTag().getInt("corner1z");
        return new BlockPos(x,y,z);
    }
    public BlockPos getCorner2(ItemStack stack){
        int x = stack.getOrCreateTag().getInt("corner2x");
        int y = stack.getOrCreateTag().getInt("corner2y");
        int z = stack.getOrCreateTag().getInt("corner2z");
        return new BlockPos(x,y,z);
    }

    public void swapArea(ItemUseContext context) {

        PlayerEntity player = context.getPlayer();
        ItemStack stack = context.getItem();


        int x1 = stack.getOrCreateTag().getInt("corner1x");
        int y1 = stack.getOrCreateTag().getInt("corner1y");
        int z1 = stack.getOrCreateTag().getInt("corner1z");
        int x2 = stack.getOrCreateTag().getInt("corner2x");
        int y2 = stack.getOrCreateTag().getInt("corner2y");
        int z2 = stack.getOrCreateTag().getInt("corner2z");


        AxisAlignedBB area = new AxisAlignedBB(x1, y1, z1, x2, y2, z2);
        //Check to see if more than 1 million blocks are selected.
        //TODO This is probably not where this check should be. Move it in with the other checks
        if ((area.maxX - area.minX) * (area.maxY - area.minY) * (area.maxZ - area.minZ) > MAXIMUM_BLOCKS) {
            assert player != null;
            player.sendMessage(new StringTextComponent("Selected Area is Too Large. Aborting"));

        }
        else
            {

            //state = SWAPPING;
            stack.getOrCreateTag().putInt("state", SWAPPING);



            }
    }

    BlockPos getNextSwapPos(World world,ItemStack stack){
        int lastXVisited = stack.getOrCreateTag().getInt("lastX");
        int lastYVisited = stack.getOrCreateTag().getInt("lastY");
        int lastZVisited = stack.getOrCreateTag().getInt("lastZ");
        int x1 = stack.getOrCreateTag().getInt("corner1x");
        int y1 = stack.getOrCreateTag().getInt("corner1y");
        int z1 = stack.getOrCreateTag().getInt("corner1z");
        int x2 = stack.getOrCreateTag().getInt("corner2x");
        int y2 = stack.getOrCreateTag().getInt("corner2y");
        int z2 = stack.getOrCreateTag().getInt("corner2z");
        AxisAlignedBB area = new AxisAlignedBB(x1, y1, z1, x2, y2, z2);


           int z = lastZVisited;
           int y = lastYVisited;
          int  x = lastXVisited + 1;


        //for (int z = (int) area.minZ; z <= area.maxZ; z++) {
        while( z <= area.maxZ){
            while( y <= area.maxY){
                while( x <= area.maxX){





            //Texturizer.LOGGER.info("looking for next target block at >> {} {} {}", x,y,z);


                    BlockPos pos = new BlockPos(x,y,z);
            if (isSwapTarget(world, pos)) {
                return pos;
            }
            x++;
            }
                y++;
                x = (int) area.minX;
                }
                    z++;
                    y= (int) area.minY;
                        }

        stack.getOrCreateTag().putInt("state",SELECTING_CORNER_ONE);
        return new BlockPos((int) area.minX -1,(int) area.minY, (int) area.minZ); //Starts it at the beginning for next time

    }
    public boolean withinArea(ItemStack stack, BlockPos pos){
        int x1 = stack.getOrCreateTag().getInt("corner1x");
        int y1 = stack.getOrCreateTag().getInt("corner1y");
        int z1 = stack.getOrCreateTag().getInt("corner1z");
        int x2 = stack.getOrCreateTag().getInt("corner2x");
        int y2 = stack.getOrCreateTag().getInt("corner2y");
        int z2 = stack.getOrCreateTag().getInt("corner2z");
        boolean ValidArea = stack.getOrCreateTag().getBoolean("corner2Valid");
        AxisAlignedBB area = new AxisAlignedBB(x1, y1, z1, x2, y2, z2);

        return(ValidArea && area.minX <= pos.getX() && pos.getX() <= area.maxX && area.minY <= pos.getY() && pos.getY() <= area.maxY && area.minZ <= pos.getZ() && pos.getZ() <= area.maxZ);
    }


}