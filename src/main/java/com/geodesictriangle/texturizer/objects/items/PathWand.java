package com.geodesictriangle.texturizer.objects.items;

import com.geodesictriangle.texturizer.util.DestroyHelper;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.BushBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;


@MethodsReturnNonnullByDefault
public class PathWand extends AbstractSwapWand {
    public PathWand(Properties properties) {
        super(properties);
    }

    public ActionResultType onItemUse(ItemUseContext context) {
        World world = context.getWorld();
        BlockPos pos = context.getPos();
        PlayerEntity player = context.getPlayer();
        ItemStack stack = context.getItem();


        if (!world.isRemote) {



                for(int x = pos.getX()-1; x <= pos.getX()+1; x++){
                        for(int z = pos.getZ()-1; z <= pos.getZ()+1; z++) {
                            BlockPos swappos = new BlockPos(x, pos.getY(), z);
                            if(isSwapTarget(world,swappos)){
                                Block nextBlock = getNextBlock(stack,player);
                                if (nextBlock != Blocks.AIR){
                                    swapBlock(world,swappos,!player.abilities.isCreativeMode,nextBlock);
                                }
                            }
                        }
                }




            }
        return ActionResultType.SUCCESS;
    }

    public Block getNextBlock(ItemStack stack, PlayerEntity player){
        Integer mode = stack.getOrCreateTag().getInt("mode");
        List<Block> blocklist = getBlockList(player, stack);

        if(blocklist.size() > 0) {
            int randomblock = (int) (Math.random() * blocklist.size());
            Block block = blocklist.get(randomblock);

            if(block != Blocks.AIR){
                if(player.abilities.isCreativeMode){
                    return block;
                }

                boolean HasBlock = false;

                if(mode.equals(INTERNAL)){
                    HasBlock = wandInvRemoveItem(stack, block.asItem()); // Also removes item
                }
                if(mode.equals(HOTBAR) || !HasBlock) {
                    HasBlock = playerInvRemoveItem(player, block.asItem()); // Also removes item
                }

                if(!HasBlock){
                    player.sendMessage(new StringTextComponent("Ran Out of "+ block.asItem() ));
                    return Blocks.AIR;
                }
                return block;
            }

        }
        return Blocks.AIR;
        //randomblock = 1;
        //return Blocks.STONE;

    }

    public void swapBlock(World world, BlockPos pos, boolean dropBlock, Block swapblock){
        if(!world.isRemote) {
            DestroyHelper.destroyBlockNoParticles(pos,dropBlock,(Entity) null, world);
            //world.destroyBlock(pos, dropBlock);
            world.setBlockState(pos, swapblock.getDefaultState());
        }
    }
    @Override
    boolean isSwapTarget(World world, BlockPos pos){
        return((world.getBlockState(pos).getBlock() != Blocks.AIR) && (world.getBlockState(pos).getBlock() != Blocks.BEDROCK) && !(world.getBlockState(pos).getBlock() instanceof BushBlock));
    }
}
