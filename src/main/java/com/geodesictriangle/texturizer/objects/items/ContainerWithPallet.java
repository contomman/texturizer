package com.geodesictriangle.texturizer.objects.items;


import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.util.FakeSlotItemHandler;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.registries.ObjectHolder;

import javax.annotation.Nonnull;


@MethodsReturnNonnullByDefault
public class ContainerWithPallet extends Container {
    @ObjectHolder("texturizer:texturewand" ) public static ContainerType<ContainerWithPallet> TYPE;


    public static ContainerWithPallet fromNetwork(int windowId, PlayerInventory inv, PacketBuffer buf) {
        //super(TEXTURE_WAND.get(), windowId);
        Hand hand = buf.readBoolean() ? Hand.MAIN_HAND : Hand.OFF_HAND;
        return new ContainerWithPallet(windowId, inv, inv.player.getHeldItem(hand));

    }
    private final ItemStack wand;

    public ContainerWithPallet(int windowId, PlayerInventory playerInv, ItemStack wand) {

        super(TYPE, windowId);
        this.wand = wand;
        IItemHandlerModifiable wandInv = (IItemHandlerModifiable) wand.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).orElse(null);

        int slotsize = 18;
        //Wand Pallet Inventory
        int startX = 8;
        int startY = 31;
        int nRows = 4;
        int nCols = 4;

        for(int row = 0; row <nRows; ++row){
            for(int col = 0; col < nCols; ++col){
                //this.addSlot(new SlotItemHandler(wandInv,(row*nCols)+col, startX+col*slotsize, startY+row*slotsize));
                this.addSlot(new FakeSlotItemHandler(wandInv,(row*nCols)+col, startX+col*slotsize, startY+row*slotsize));


            }
        }
        //Wand Bulk Inventory
        startX = 98;
        startY = 31;
        nRows = 4;
        nCols = 8;

        for(int row = 0; row <nRows; ++row){
            for(int col = 0; col < nCols; ++col){
                this.addSlot(new SlotItemHandler(wandInv,16+(row*nCols)+col, startX+col*slotsize, startY+row*slotsize));

            }
        }

        //Player Inventory
        int startPlayerInvX = 44;
        int startPlayerInvY = 115;


        for(int row = 0; row <3; ++row){
            for(int col = 0; col < 9; ++col){
                this.addSlot(new Slot(playerInv,9+(row*9)+col, startPlayerInvX+col*slotsize, startPlayerInvY+row*slotsize));

            }
        }

        //Hotbar
        int hotbarY = startPlayerInvY+58;
        for(int col = 0; col < 9; ++col){
            this.addSlot(new Slot(playerInv,col, startPlayerInvX+col*slotsize, hotbarY));

        }

    }

    @Override
    public boolean canInteractWith(@Nonnull PlayerEntity player) {
        ItemStack main = player.getHeldItemMainhand();
        ItemStack off = player.getHeldItemOffhand();
        return !main.isEmpty() && main == wand || !off.isEmpty() && off == wand;
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if (index < 36) {
                if (!this.mergeItemStack(itemstack1, 36, this.inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemstack1, 0, 36, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }
        return itemstack;
    }


    @Override
    public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, PlayerEntity player) {
        Texturizer.LOGGER.info("{}", slotId);
        if(slotId >= 0 && slotId <= 15){
            this.inventorySlots.get(slotId).putStack(player.inventory.getItemStack());
        }
        return super.slotClick(slotId,dragType,clickTypeIn,player);
    }



}

