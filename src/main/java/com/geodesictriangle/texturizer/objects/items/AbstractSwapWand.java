package com.geodesictriangle.texturizer.objects.items;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.inventory.container.SimpleNamedContainerProvider;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AbstractSwapWand extends Item {
    public AbstractSwapWand(Properties properties) {
        super(properties);
    }


    //Available Modes
    Integer HOTBAR = 0;
    Integer INTERNAL = 1;

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, @Nonnull Hand hand) {
        if (!world.isRemote) {
            ItemStack stack = player.getHeldItem(hand);
            if(player.isCrouching()) {
                INamedContainerProvider container = new SimpleNamedContainerProvider((w, p, pl) -> new ContainerWithPallet(w, p, stack), stack.getDisplayName());
                NetworkHooks.openGui((ServerPlayerEntity) player, container, buf -> buf.writeBoolean(hand == Hand.MAIN_HAND));
            }


        }

        return ActionResult.resultPass(player.getHeldItem(hand));
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return false;
    }


    @Nonnull
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, CompoundNBT oldCapNbt) {
        return new AbstractSwapWand.InvProvider();
    }

    private static class InvProvider implements ICapabilitySerializable<INBT> {

        private final IItemHandler inv = new ItemStackHandler(84) { //TODO WRONG SIZE
            @Override
            public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                return true;
            }
        };
        private final LazyOptional<IItemHandler> opt = LazyOptional.of(() -> inv);

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> capability, @Nullable Direction facing) {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.orEmpty(capability, opt);
        }

        @Override
        public INBT serializeNBT() {
            return Objects.requireNonNull(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.writeNBT(inv, null));
        }

        @Override
        public void deserializeNBT(INBT nbt) {
            CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.readNBT(inv, null, nbt);
        }
    }

    public List<Block> getBlockList(PlayerEntity player, ItemStack wand){
        List<Block> blocklist = new ArrayList<>();
        Integer mode = wand.getOrCreateTag().getInt("mode");

        if(mode.equals(HOTBAR)) {
            //For toolbar
            for (int i = 0; i <= 8; i++) {

                ItemStack testitemstack = player.inventory.getStackInSlot(i);

                if (testitemstack.getItem() instanceof BlockItem) {
                    Block block = (((BlockItem) testitemstack.getItem()).getBlock());
                    blocklist.add(block);

                }
                if (testitemstack.getItem() == Items.SALMON) {
                    blocklist.add(Blocks.AIR); //Placeholder - Means do not replace the block at that location
                }

            }
        }
        if(mode.equals(INTERNAL)) {
            //For toolbar
            int LAST_PALLET_SLOT = 15;
            IItemHandler wandInv = wand.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).orElseThrow(NullPointerException::new);

            for (int i = 0; i <= LAST_PALLET_SLOT; i++) {

                ItemStack testitemstack = wandInv.getStackInSlot(i);

                if (testitemstack.getItem() instanceof BlockItem) {
                    Block block = (((BlockItem) testitemstack.getItem()).getBlock());
                    blocklist.add(block);

                }
                if (testitemstack.getItem() == Items.SALMON) {
                    blocklist.add(Blocks.AIR); //Placeholder - Means do not replace the block at that location
                }

            }
        }


        return blocklist;
    }

    public boolean playerInvRemoveItem(PlayerEntity player, Item item){
        //check non hotbar slots first
        for(int i = 9; i < player.inventory.getSizeInventory() ; i++) {

            ItemStack itemstack = player.inventory.getStackInSlot(i);
            if(itemstack.getItem() == item){
                if(itemstack.getCount() > 0){
                    itemstack.shrink(1);
                    return true;
                }
            }
        }
        //check toolbar slots, leave 1 left in stack
        for(int i = 0; i <= 8 ; i++) {

            ItemStack itemstack = player.inventory.getStackInSlot(i);
            if(itemstack.getItem() == item){
                if(itemstack.getCount() > 1){
                    itemstack.shrink(1);
                    return true;
                }
            }

        }
        return false;
    }

    public boolean wandInvRemoveItem(ItemStack wand, Item item){

        IItemHandler wandInv = wand.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).orElseThrow(NullPointerException::new);


        //check non hotbar slots first
        int FIRST_NON_PALLET_SLOT = 16;
        for(int i = FIRST_NON_PALLET_SLOT; i < wandInv.getSlots() ; i++) {

            ItemStack itemstack = wandInv.getStackInSlot(i);
            if(itemstack.getItem() == item){
                if(itemstack.getCount() > 0){
                    //wandInv.extractItem(i,1,true);
                    itemstack.shrink(1);

                    return true;

                }
            }
        }

        return false;
    }

    boolean isSwapTarget(World world, BlockPos pos){
        return((world.getBlockState(pos).getBlock() != Blocks.AIR) && world.getBlockState(pos).getBlock() != Blocks.BEDROCK);
    }

    public void toggleMode(PlayerEntity player, ItemStack stack){
        int mode = stack.getOrCreateTag().getInt("mode");
        if (mode == INTERNAL) {
            stack.getOrCreateTag().putInt("mode", HOTBAR);
            player.sendMessage(new StringTextComponent("Taking pallet from hotbar"));

        }
        if (mode == HOTBAR) {
            stack.getOrCreateTag().putInt("mode", INTERNAL);
            player.sendMessage(new StringTextComponent("Taking pallet from Internal Inventory"));
        }

    }

}
