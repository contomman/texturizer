package com.geodesictriangle.texturizer;

import com.geodesictriangle.texturizer.clientrender.GuideRenderer;
import com.geodesictriangle.texturizer.init.BlockInit;
import com.geodesictriangle.texturizer.init.ItemInit;
import com.geodesictriangle.texturizer.init.ModContainerTypes;
import com.geodesictriangle.texturizer.init.ModTileEntityTypes;
import com.geodesictriangle.texturizer.network.PacketHandler;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.stream.Collectors;

import static com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.BuildButtonModule.BUILD_TEXTURE;
import static com.geodesictriangle.texturizer.tileentities.BuildGuide.Modules.OriginButtonModule.*;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Texturizer.MODID)
@Mod.EventBusSubscriber(modid = Texturizer.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
@MethodsReturnNonnullByDefault
public class Texturizer
{
    public static final String MODID = "texturizer";
    public static Texturizer instance;
    // Directly reference a log4j logger.
    public static final Logger LOGGER = LogManager.getLogger();

    public Texturizer() {
        instance = this;
        IEventBus modeventbus = FMLJavaModLoadingContext.get().getModEventBus();

        // Register the setup method for modloading
        modeventbus.addListener(this::setup);
        // Register the enqueueIMC method for modloading
        modeventbus.addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        modeventbus.addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        modeventbus.addListener(this::doClientStuff);

        //register items
        ItemInit.ITEMS.register(modeventbus);
        //register blocks
        BlockInit.BLOCKS.register(modeventbus);
        //register tile entities
        ModTileEntityTypes.TILE_ENTITY_TYPES.register(modeventbus);

        //register containers

        ModContainerTypes.CONTAINER_TYPES.register(modeventbus);
        PacketHandler.registerMessages();


        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);




    }

    private void setup(final FMLCommonSetupEvent event)
    {
        // some preinit code
        //LOGGER.info("HELLO FROM PREINIT");
        //LOGGER.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
        //LOGGER.info("My Item >> {}", textureWand.getRegistryName());
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client
        //LOGGER.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
        GuideRenderer.register();
    }

    @SubscribeEvent
    public static void onTextureStitch(TextureStitchEvent.Pre event){
        if (!event.getMap().getTextureLocation().equals(AtlasTexture.LOCATION_BLOCKS_TEXTURE)){
            return;
        }
        event.addSprite(ARROW_TEXTURE_UP);
        event.addSprite(ARROW_TEXTURE_DOWN);
        event.addSprite(ARROW_TEXTURE_LEFT);
        event.addSprite(ARROW_TEXTURE_RIGHT);

        event.addSprite(BUILD_TEXTURE);

    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        // some example code to dispatch IMC to another mod
        InterModComms.sendTo(Texturizer.MODID, "helloworld", () -> { LOGGER.info("Hello world from the MDK"); return "Hello world";});
    }

    private void processIMC(final InterModProcessEvent event)
    {
        // some example code to receive and process InterModComms from other mods
        LOGGER.info("Got IMC {}", event.getIMCStream().
                map(m->m.getMessageSupplier().get()).
                collect(Collectors.toList()));
    }
    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        //LOGGER.info("HELLO from server starting");
    }
    @SubscribeEvent
    public static void onRegisterItems(final RegistryEvent.Register<Item> event) {
        final IForgeRegistry<Item> registry = event.getRegistry();

        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> {
            final Item.Properties properties = new Item.Properties().group(texturizerItemGroup.instance);
            final BlockItem blockItem = new BlockItem(block, properties);
            blockItem.setRegistryName(Objects.requireNonNull(block.getRegistryName()));
            registry.register(blockItem);
        });

        LOGGER.debug("Registered BlockItems!");
    }


    public static class texturizerItemGroup extends ItemGroup{
        public static final texturizerItemGroup instance = new texturizerItemGroup(ItemGroup.GROUPS.length,"Texturizer");
        private texturizerItemGroup(int index, String label){
            super(index,label);
        }
        @Override
        public ItemStack createIcon(){
            return new ItemStack(ItemInit.texturewand.get());
        }

}

}
