package com.geodesictriangle.texturizer.util;


import com.geodesictriangle.texturizer.Texturizer;
import com.geodesictriangle.texturizer.client.gui.TextureWandScreen;
import com.geodesictriangle.texturizer.init.ModContainerTypes;
import net.minecraft.client.gui.ScreenManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;


@Mod.EventBusSubscriber(modid = Texturizer.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {


    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event) {
        ScreenManager.registerFactory(ModContainerTypes.TEXTURE_WAND.get(), TextureWandScreen::new);

    }


}