package com.geodesictriangle.texturizer.util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class DestroyHelper {
    public static void destroyBlockNoParticles(BlockPos p_225521_1_, boolean p_225521_2_, @Nullable Entity p_225521_3_, World world) {
        // Exact copy of world.destroyBlock
        // Removed creation of particles
        BlockState blockstate = world.getBlockState(p_225521_1_);
        if (blockstate.isAir(world, p_225521_1_)) {
        } else {
            IFluidState ifluidstate = world.getFluidState(p_225521_1_);
            //Below line is what makes the particles
            //world.playEvent(2001, p_225521_1_, Block.getStateId(blockstate));
            if (p_225521_2_) {
                TileEntity tileentity = blockstate.hasTileEntity() ? world.getTileEntity(p_225521_1_) : null;
                Block.spawnDrops(blockstate, world, p_225521_1_, tileentity, p_225521_3_, ItemStack.EMPTY);
            }

            world.setBlockState(p_225521_1_, ifluidstate.getBlockState(), 3);
        }
    }
}
