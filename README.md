# Texturizer for Minecraft 1.15
A Mod that adds the Texture Wand, which swaps out an area of blocks using a user-selected pallet.
## Texture Wand
### Usage
* Shift+RightClick to select an area
* RightClick on the selected area to begin the swap
* m (Configurable) to switch between Hotbar mode and Internal Inventory mode
* Shift+RightClick in the air to open the internal inventory

